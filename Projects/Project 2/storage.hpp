#ifndef _STORAGE_HPP
#define _STORAGE_HPP

#include <string>
using namespace std;

bool IsFull         ( const int itemCount, const int ARRAY_SIZE );
bool IsValidIndex   ( const int index, const int itemCount, const int ARRAY_SIZE );
void AddItem        ( const string item, string arr[], int & itemCount, const int ARRAY_SIZE );
void UpdateItem     ( const int index, const string newItem, string arr[], const int itemCount, const int ARRAY_SIZE );
void ClearAllItems  ( string arr[], int & itemCount, const int ARRAY_SIZE );
void ViewAllItems   ( const string arr[], const int itemCount );
void SaveItems      ( const string filepath, const string arr[], const int itemCount );
void LoadItems      ( const string filepath, string arr[], int & itemCount );

#endif
