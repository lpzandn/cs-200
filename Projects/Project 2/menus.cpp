#include "menus.hpp"

#include <iostream>
#include <fstream>
#include <limits>
using namespace std;


/**
Gets integer input from the user and ensures that the input is between
a certain valid range before returning it.
@param min  The minimum number allowed.
@param max  The maximum number allowed.
@return The user's input, between min and max (inclusive).
*/
int GetChoice( int min, int max )
{
    int choice;
    cout << "(" << min << " - " << max << ") >> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid choice, try again." << endl;
        cout << ">> ";
        cin >> choice;
    }

    return choice;
}

/**
Displays the main menu of the program as well as a count of total items.
@param savedItems  The total amount of items that have been stored.
*/
void DisplayMainMenu( int savedItems )
{
    cout << endl << "---------------------------------" << endl;
    cout << "MAIN MENU" << endl
         << savedItems << " total item(s)" << endl << endl
         << "---------" << endl
         << "1. Add item" << endl
         << "2. Update items" << endl
         << "3. Clear all items" << endl
         << "4. View all items" << endl
         << "5. Save and quit" << endl;
}

/**
Clears the screen
*/
void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

/**
Pauses before continuing
*/
void Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
    #endif
}

