#ifndef _MENUS_HPP
#define _MENUS_HPP

#include <string>
using namespace std;

void DisplayMainMenu( int savedItems );
int GetChoice( int min, int max );
void ClearScreen();
void Pause();

#endif
