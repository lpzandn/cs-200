#include <iostream>     // Used for cin and cout
#include <iomanip>      // Used for formatting cout statements
#include <string>       // Used for string data types
#include <cstdlib>      // Used for rand()
#include <ctime>        // time used to seed random number generator
using namespace std;

void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location );

int main()
{
    /******************************************************************* INITIALIZE VARIABLES */
    bool done = false;                      // Flag for if the game is over
    bool successfulAction = false;          // Flag for if the user entered valid input
    int food = 5;                           // Player stat - how much food they have
    int maxHealth = 20;                     // Player stat - their maximum amount of health
    int health = maxHealth;                 // Player stat - their current health amount
    string name = "ME";                     // Player stat - what their name is
    string location = "Overland Park";      // Player stat - what location they're in
    int day = 1;                            // Player stat - how many days it has been
    string dump;                            // A variable to dump input into
    int choice;                             // A variable to dump input into
    int mod;                                // A variable used for calculations later

    srand( (unsigned int) time( NULL ) );                  // Seeding the random number generator

    /******************************************************************* GAME START */
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline( cin, name );

    cout << endl;

    /******************************************************************* GAME LOOP */
    while ( !done )
    {
        /** Start of the round **/
        successfulAction = false;
        DisplayMenu( day, food, health, maxHealth, name, location );
        cout << "CHOICE: ";
        cin >> choice;

        /** Student implements features here **/

 
        switch (choice)
              {
                  case 1:
                  {
                      cout << "You Scavenge Here." << endl;
                      successfulAction = true;
                      int randomChance = rand() % 5;
                    //USE AS CONTROL TO TEST VARIABLES  int randomChance = 1;
                      if (randomChance == 0)
                      {
                          //this mod controls and amount of hunger gained when finding food (2 - 5)
                          mod = rand () % 4 + 2;
                          food += mod;
                          cout << "You find a stash of food (+" << mod << "Food" << endl;
                      }
                      else if (randomChance == 1)
                      {
                          //this mod controls and amount of health lost from the zombie attack (2 - 8)
                          mod = rand () % 7 + 2;
                          health -= mod;
                          cout << "A zombie surprises you!" << endl << "You get hurt in the encounter. (-" << mod << "health)" << endl;
                      }
                      else if (randomChance == 2)
                    {
                        //this mod controls and amount of health gained from finding medical supplies(2 - 6)
                        mod = rand () % 4 + 2;
                        health += mod;
                        cout << "You found some medical supplies. (+ " << mod << " health)" << endl;
                    }
                      else if (randomChance == 3)
                      {
                          //this mod controls and amount of food lost due to scavengers (2 - 6)
                          mod = rand () % 5 + 2;
                          food -= mod;
                          cout << "Another scavenger ambushes you!" << endl << "They take some supplies from you. (-" << mod << " food)" << endl;
                      }
                      else if (randomChance == 4)
                      {
                          cout << "You dont find anything" << endl;
                      }
                  }
                      if (successfulAction == true)
                      {
                          break;
                      }
                      else
                      {
                          continue;
                      }
                  case 2:
                  {
                      
                      cout << "Walk to where?" << endl << "1. Overland Park" << endl << "2. Raytown" << endl << "3. Kansas City" << endl << "4. Gladstone" << endl;
                      
                      cin >> choice;
                      if (choice == 1 || choice == 2 || choice == 3 || choice == 4)
                      {
    
                          if (choice == 1 && location != "Overland Park")
                            {
                                cout << "You chose Overland Park" << endl;
                                string location = "Overland Park";
                                successfulAction = true;
                            }
                          else if (choice == 2 && location != "Raytown")
                            {
                                cout << "You chose Raytown" << endl;
                                string location = "Raytown";
                                successfulAction = true;
                            }
                          else if (choice == 3 && location != "Kansas City")
                            {
                                cout << "You chose Kansas City" << endl;
                                string location = "Kansas City";
                                successfulAction = true;
                            }
                          else if (choice == 4 && location != "Gladstone")
                            {
                                cout << "You chose Gladstone" << endl;
                                string location = "Gladstone";
                                successfulAction = true;
                            }
                          else
                            {
                                cout << "You're already there!" << endl;
                                successfulAction = false;
                            }
                      }
                      else
                      {
                          cout << "Invalid Choice";
                      successfulAction = false;
                          }
                  }
                      if (successfulAction == true)
                      {
                          int randomChance = rand() % 5;
                          
                          if (randomChance == 0)
                          {
                              cout << "A zombie attacks!" << endl << "You fight it off." << endl;
                          }
                          else if (randomChance == 1)
                          {
                              mod = rand () % 5 + 2;
                              cout << "A zombie attacks!" << endl << " It bites you as you fight it! (-" << mod << " Health)" << endl;
                              health -= mod;
                          }
                        else if ( randomChance == 2)
                        {
                            mod = rand () % 3 + 2;
                            cout << "You find another scavenger and trade goods. (+" << mod << " food" << endl;
                            food += mod;
                        }
                          else if (randomChance == 3)
                          {
                              mod = rand () % 4 + 2;
                              cout << "You find a safe building to rest in temporarily. (+" << mod << " health)" << endl;
                              health += mod;
                          }
                          else
                          {
                              cout << "The journey was uneventful." << endl;
                          }
                          break;
                      }
                      else
                      {
                          continue;
                      }
               
              }
        
        day++ ;
        cout << "The day passes (+1 day)" << endl;
        if (food > 0)
        {
            cout << "You eat a meal (-1 food) (+1 health)" << endl;
            food -= 1;
            health += 1;
        }
        else
        {
          
           //this mod controls and amount of hunger lost when starving
            mod = rand () % 4 + 1;
            cout << "You are starving!";
            cout << " (-" << mod << " health)" << endl;
            health -= mod;
        }
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        
        if (food <= 0)
        {
            food = 0;
        }
        if (health <= 0)
        {
            cout << "You have died." << endl;
            done = true;
        }
        else if (day == 20 )
        {
            cout << "In the morning, a group of scavengers find you. They have fortification nearby and are rebuilding a society. you agree to live in their town" << endl;
            done = true;
        }
       
        /** End of the round - nothing to update **/
        cout << endl << "Press ENTER to continue...";
        cin.ignore();
        getline( cin, dump );

        cout << "----------------------------------------" << endl;
        cout << "----------------------------------------" << endl;
    }

    /******************************************************************* GAME OVER */
    cout << endl << endl << "You survived the apocalpyse on your own for " << day << " days." << endl;

    return 0;
}

/* You don't need to modify this, it just displays the user's information at the start of each round. */
void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location )
{
    cout << "----------------------------------------" << endl;
    cout << left << setw( 3 ) << "-- " << setw( 35 ) << name << "--" << endl;
    cout << left
        << setw( 6 ) << "-- Day "   << setw( 4 ) << day
        << setw( 6 ) << "Food: "    << setw( 4 ) << food
        << setw( 8 ) << "Health: "  << setw( 2 ) << health << setw( 1 ) << "/" << setw( 2 ) << maxHealth
        << right << setw( 6 ) << "--" << endl;
    cout << left
        << setw( 10 ) << "-- Location: " << setw( 20 ) << location
        << right << setw( 7 ) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elseware                 --" << endl;
    cout << "----------------------------------------" << endl;
    for ( int i = 0; i < 15; i++ )
    {
        cout << endl;
    }
}
