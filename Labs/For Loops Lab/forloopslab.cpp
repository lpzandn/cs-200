//
//  main.cpp
//  Forloopslab
//
//  Created by AndresLopezBormann on 9/16/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    for (int i = 0; i <= 20 ; i++)
        cout << i << " ";
}

void Program2()
{
    
    for (int i = 1; i <= 128 ; i= i * 2)
        cout << i << " ";
        
}

void Program3()
{
    int i;
    int n;
    int sum = 0;
    cout << "Enter a value for n: ";
    cin >> n;
    for (i = 1 ; i <= n; i++){
        sum += i;
        cout << "sum: "<<sum << endl;}
}

void Program4()
{
    string text;
    char lookForLetter;
    int letterCount = 0;
    int i;
    
    cout << "Enter a phrase: ";
    cin.ignore();
    getline (cin, text);
    
    cout << "enter a letter";
    cin >> lookForLetter;
    
    for (i = 0; i < text.size(); i++)
    {cout << "Letter " << i << ": " << text [i] << endl;
 if ( tolower(text [i]) == (lookForLetter) )
 { letterCount++;
 }}
    cout << "There are " << letterCount << " "<< lookForLetter << "(s) in "  << text;

}


int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-4): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}

