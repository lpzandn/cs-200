#ifndef Student_h
#define Student_h

#include <string>
using namespace std;
class Student{

public:
    Student();
    void Setup(string newName);
    void AddGrade (float score);
    float GetGPA ();
    string GetName();
    void Display();
    
    
private:
    string fullname;
    float grades[10];
    int totalgrades;

};

Student::Student()
{
    fullname = "no name";
    totalgrades = 0;
}

void Student:: Setup(string newName)
{
    fullname = newName;
    
}

void Student:: AddGrade(float score)
{
    if (score < 0 || score > 4)
    {
        cout << "Score is out of range" << endl;
        return;
    }
    
    if (totalgrades == 10)
    {
        cout << "Grade array is full!" << endl;
        return;
    }
    grades[totalgrades] = score;
    totalgrades++;
}

float Student::GetGPA()
{
    float sum = 0;
    for (int i=0; i < totalgrades;i++)
    {
        sum += grades[i];
    }
    float avg = sum / totalgrades;
    return avg; // <<avg GPA
    
}
string Student::GetName()
{
    return fullname;
}
void Student::Display()
{
    cout << GetName() << ": " << GetGPA() << endl;
}

#endif /* Student_h */
