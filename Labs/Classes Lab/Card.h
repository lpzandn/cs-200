#ifndef Card_h
#define Card_h


using namespace std;
#include <string>

class Card
{
    
public:
    void Setup(string newRank, char newSuit);
    void Display();
    void DisplayName();
    string GetRank();
    char GetSuit();
    
private:
    string m_rank;
    char m_suit;
};

void Card::Setup(string newRank, char newSuit)
{
    m_rank = newRank;
    m_suit = newSuit;
}

void Card::Display()
{
    cout << m_rank << " - " << m_suit << endl ;
}
void Card::DisplayName()
{
    if (m_rank == "Q")
        {cout << "Queen";}
    else if (m_rank == "K")
        {cout << "King";}
    else if (m_rank == "A")
        {cout << "Ace";}
    else if (m_rank == "J")
        {cout << "Jack";}
    else
        {cout << m_rank;}
    
    cout << " of ";
    
    if (m_suit == 'H')
        {cout << "Hearts";}
    else if (m_suit == 'S')
        {cout << "Spades";}
    else if (m_suit == 'C')
        {cout << "Clubs";}
    else if (m_suit == 'D')
        {cout << "Diamond";}
}
string Card::GetRank()
{
    return m_rank;
}
char Card::GetSuit()
{
    return m_suit;
}


    
    /*{
        const int SUITS = 4;
        const int RANKS = 13;
        
        const char suits[] = {'C', 'D', 'H', 'S'};
        const string ranks[] = {"A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
        
        for(int s = 0; s< SUITS; s++)
        {
            for (int r  = 0; r< RANKS; r++)
            {
                char suit = suits[s];
                
                string rank = ranks[r];
                
            }
        }*/
#endif /* Card_h */
