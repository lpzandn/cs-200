#include <iostream>
#include <string>

#include "Student.h"
#include "Die.h"
#include "Card.h"
#include "Deck.h"
#include "Ingredient.h"
#include "Recipe.h"

using namespace std;
int main()
{
    bool done = false;
    while (done == false)
    {
        cout << "Run Program (1-4): ";
        int choice;
        cin >> choice;
        
    if (choice == 1){
        Die d20(20);
        int hitRoll = d20.roll();
        Die d8(8);
        int damageRoll = d8.roll();
        
        cout << hitRoll << " to hit, for " << damageRoll << " Damage" << endl;
    }
    
    else if (choice == 2){
        Student myStudent;
        cout << "What is the student's name?" << endl;
        string name;
        cin >> name;
        
        myStudent.Setup(name);
        
        int gradesEntered;
        cout << "# of Grades Entered: ";
        cin >> gradesEntered;
        
        for (int i = 0; i < gradesEntered; i++)
        {
            float grade;
            cout << "Enter a grade for 0.0 - 4.0: ";
            cin >> grade;
            myStudent.AddGrade(grade);
        }
        cout << endl << endl;
        myStudent.Display();
    }
    else if (choice == 3){
        Deck myDeck;
        
        int suit, rank;
        
        cout << "Enter a suit (0 - 3): ";
        cin >> suit;
        
        cout << "Enter a rank (0 - 12): ";
        cin >> rank;
        
        myDeck.DisplayCard(suit, rank);
        
        suit = rand() % 4;
        rank = rand() % 13;
        myDeck.DisplayCard(suit, rank);

    }
    else if (choice == 4)
    {
        Recipe  cookies;
        cookies.SetName( "Chocolate  Chip  Cookies" );
        cookies.AddIngredient( "Butter", 1, "cup" );
        cookies.AddIngredient( "White  sugar", 1, "cup" );
        cookies.AddIngredient( "Eggs", 2, "items" );
        cookies.AddIngredient( "Vanilla", 2, "teaspoons" );
        cookies.AddIngredient( "Baking  soda", 1, "teaspoon" );
        
        cookies.SetInstructions( "Preheat  oven to 350 F, combine ingredients  in bowl , put on  cookie  sheet , bake  for 10  minutes.");
        
        cookies.Display ();
    }
        
        cout << "close program? (y/n)";
        char close;
        cin >> close;
        if (close == 'y')
        {
            done = true;
        }
        else if (close == 'n')
        {
            done= false;
        }
    }
    return 0;
}

