//
//  Recipe.h
//  Classes Lab
//
//  Created by AndresLopezBormann on 12/2/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#ifndef Recipe_h
#define Recipe_h

#include <string>
using namespace std;

class Recipe{
public:
    Recipe();
    void SetName(string name);
    void SetInstructions(string instructions);
    void AddIngredient(string name, float amount, string unit);
    void Display();
    
private:
    string m_name;
    string m_instructions;
    Ingredient m_ingredients[10];
    int m_totalIngredients;
};

Recipe::Recipe()
{
    m_totalIngredients = 0;
}
void Recipe::SetName(string name)
{
    m_name = name;
}
void Recipe::SetInstructions(string instructions)
{
    m_instructions = instructions;
}
void Recipe::AddIngredient(string name, float amount, string unit)
{
    if (m_totalIngredients == 10)
    {
        cout << "Error, Ingredient list is full" << endl;
        return;
    }
    m_ingredients[m_totalIngredients].Setup(name, amount, unit);
    m_totalIngredients++;
}
void Recipe::Display()
{
    cout << m_name << endl << endl ;
    for (int i = 0; i < m_totalIngredients ; i++)
    {
        m_ingredients[i].Display();
    }
}

#endif /* Recipe_h */
