//
//  Ingredient.h
//  Classes Lab
//
//  Created by AndresLopezBormann on 12/2/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#ifndef Ingredient_h
#define Ingredient_h

#include <string>
using namespace std;

class Ingredient
{
public:
    void Setup(string name, float amount, string unit);
    void Display();
    
private:
    string m_name;
    string m_unit;
    float m_amount;
};
void Ingredient::Setup(string name, float amount, string unit)
{
    m_name = name;
    m_amount = amount;
    m_unit = unit;
}

void Ingredient::Display()
{
    cout << m_amount << " " << m_unit << " of " << m_name << endl;
}



#endif /* Ingredient_h */
