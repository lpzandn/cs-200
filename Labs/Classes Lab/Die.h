#ifndef Die_h
#define Die_h
#include <iostream>
#include <cstdlib>
struct Die {
    int sides;
    
    Die()
    {
        sides = 6;
    }
    Die( int sideCount){
        sides = sideCount;
    }
    
    int roll(){
        if (sides == 10)
        {
            return rand()% sides;
        }
        else
        {
        return rand() % sides + 1;
        }
    }
    
    int numSides();
    
        
};

#endif
