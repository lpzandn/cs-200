#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void Program1()
{
    const int MAX_COURSES = 6;
    int courseCount = 0;
    string myCourses [MAX_COURSES];
    //stores classes names
    for (int i = 0; i < MAX_COURSES; i++)
    {
        string userInput;
        cout << "Enter class " << i + 1 << ", or type STOP to finish: ";
        cin >> userInput;
        if (userInput == "STOP")
            break;
        else
        {
            myCourses[i] = userInput;
            courseCount++;
        }
        
    }
    //outputs classes
    cout << "Your classes:\n";
    for (int i = 0; i < courseCount; i++)
    {
        cout << myCourses [i] << "\t";
    }
   
        
        }

void Program2()
{
    const int MAX_ITEMS = 5;
        int itemCount = 0;
        string itemsOnSale [MAX_ITEMS];
        string itemsPrice [MAX_ITEMS];
        //stores item names
        for (int i = 0; i <= MAX_ITEMS; i++)
        {
           string userInput;
           cout << "Enter Item " << i+1 <<"'s Name " << ", or type STOP to finish: ";
           cin >> userInput;
           if (userInput == "STOP")
               break;
           else
           {
               itemsOnSale [i] = userInput;
           }
            
            cout << "Enter Item's Price: ";
            cin >> userInput;
            {
                itemsPrice [i] = userInput;
            }
               itemCount++;
           }
    
    ofstream output;
    output.open ("items.txt");
    //outputs classes
       output << "Items On Sale:\n";
       for (int i = 0; i < itemCount; i++)
       {
           output << i + 1 << ": " <<  itemsOnSale [i] << ":\t" << itemsPrice [i] << "\n";
       }
}

void Program3()
{
    const string daysOfWeek [7] = {
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    };
    
    const int DAYS_IN_WEEK = 7;
    const int HOURS_IN_DAY = 24;
    
    string todo [DAYS_IN_WEEK] [HOURS_IN_DAY];
    int todoItemCount = 0;
    int userInput;
    cout << "TO DO LIST\n\n" << "1. Add new to-do item\n" << "2. Save list\n" << "3. Quit\n";
    cin >> userInput;
    if (userInput == 1)
       {
    cout << "NEW ITEM\n\n0. Sunday\t\t1. Monday\n" << "2. Tuesday\t\t3. Wednesday\n" << "4. Thursday\t\t5. Friday\n" << "6. Saturday\n" << "Which day of the week (0-6):\t\t";
           
           int dayOfWeek;
           int timeOfDay;
           
           cin >> dayOfWeek;
           cout <<"\nwhat time of day? (0-23):\t\t";
           cin >> timeOfDay;
           
           cout << "What is the to do item?\t\t";
           cin.ignore();
           getline (cin, todo [DAYS_IN_WEEK] [HOURS_IN_DAY]);
           cout << "\nADDED";
       }
    else if (userInput == 2)
    {
        for (int day = 0; day < 7; day++)
        {
            cout << daysOfWeek[day] << endl;
            
            for (int hour = 0; hour < 24; hour++)
            {
                
            }
        }
    }
    else if (userInput == 3)
    {
        return;
    }
}

int main()
{
    bool done = false;

    while ( !done )
    {
        int choice;
        cout << "0. QUIT" << endl;
        cout << "1. Program 1" << endl;
        cout << "2. Program 2" << endl;
        cout << "3. Program 3" << endl;
        cout << endl << ">> ";
        cin >> choice;

        switch( choice )
        {
            case 0: done = true; break;
            case 1: Program1(); break;
            case 2: Program2(); break;
            case 3: Program3(); break;
        }
    }
    
    return 0;
}
