//
//  main.cpp
//  Branching Lab
//
//  Created by AndresLopezBormann on 9/5/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

void Program1()
{
    bool done = false;
    while ( !done )
    {
        cout << "Hometown" << endl;
       
        cout << "Enter your hometown: ";
        string hometown = "hometown";
        cin >> hometown;
        float townsize = hometown.size();
        if (townsize > 6)
            cout << "Thats a long name!\n";
        cout << "Enter your name: ";
        string name = "name";
        cin >> name;
        cout << "Hello " << name << " from " << hometown <<"!\n";
        
        
        cout << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}

void Program2()
{
    bool done = false;
    while ( !done )
    {
        cout << "Pass/Fail" << endl;
       
        float userPoints;
        float totalPoints;
        float grade;
       
        cout << "How many points does the assignment have?   ";
        cin >> totalPoints;
        cout << "How many points did you get?   ";
        cin >> userPoints;
        grade = userPoints / totalPoints;
        cout << "Score:   " << grade << "\n";
        if (grade >= .6)
        {
            cout << "You passed!";
        }
        else
        {
            cout << "You failed";
        }
        
        
        

        
        cout << "\nRun again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}
void Program3()
{
    bool done = false;
    while ( !done )
    {
        cout << "Battery Charge" << endl;
       
        cout << "Enter your phone charge: ";
        int charge;
        cin >> charge;
        if (charge >= 75){
            cout << "[****]";
        } else if (charge >= 50){
            cout << "[***_]";
        } else if (charge >= 25){
            cout << "[**__]";
        } else if (charge >= 5){
            cout << "[*___]";
        } else{
            cout << "[____]";
        }
        
        cout << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' ){
            done = true;
        }
    }
}

void Program4()
{
    bool done = false;
    while ( !done )
    {
        cout << "Input Validation" << endl;
       
        cout << "What is your favorite type of book?" << endl << "1. Scifi" << endl << "2. Historical" << endl << "3. Fantasy" << endl << "4. DIY" << endl;
        int userchoice;
        cout << "Your Choice: ";
        cin >> userchoice;
        if (userchoice >= 1 && userchoice <= 4){
            cout << "\nGood Choice!";
        } else
        {cout << "\nInvalid choice";
        }

        
        cout << "\nRun again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}

void Program5()
{
    bool done = false;
    while ( !done )
    {
        cout << "Calculator" << endl;
       
        float num1;
        float num2;
        float result;
        char op = '0';
        cout<< "Enter first number: ";
        cin >> num1;
        cout << endl << "Enter second number: ";
        cin >> num2;
        cout << endl << endl << "Which type of operation?" << endl << "+: add" << endl << "-: subtract" << endl << "*: multiply" << endl << "/: divide" << endl << endl << "Your choice: " ;
        cin >> op;
switch ( op )
{
    case '+':
        result = num1 + num2;
        cout << endl << "Result: " << result << endl << endl ;
        break;
    case '-':
        result = num1 - num2;
        cout << endl << "Result: " << result << endl << endl ;
        break;
    case '*':
        result = num1 * num2;
        cout << endl << "Result: " << result << endl << endl ;
        break;
    case '/':
        result = num1 / num2;
        if (num2 == 0)
        {
            cout << "cant divide by 0" << endl;
        }
        else
        {cout << endl << "Result: " << result << endl << endl ;}
    default:
        result = 0;
        break;

}
        
        
        cout << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
