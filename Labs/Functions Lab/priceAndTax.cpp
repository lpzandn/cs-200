//
//  main.cpp
//  Functionslab2
//
//  Created by AndresLopezBormann on 9/23/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <iomanip>
using namespace std;

//Comment out setprecision
/*int main()
{
// Declare/define funciton here
float priceA  = 9.12345;
float priceB = 10;

cout << fixed << setprecision(2)
    << "$" << priceA << endl
    << "$" << priceB << endl;
 }*/


float AddTax (float dollar)
{
    dollar = dollar * 1.12;
    return dollar;
}
int main()
{
    cout << "PRICE PLUS TAX" << endl;
    
    cout << "Price: $" << 9.99
        << "\t with tax: $" << AddTax(9.99) << endl;
    
    cout << "Price: $" << 19.95
    << "\t with tax: $" << AddTax(19.95) << endl;
    
    cout << "Price: $" << 10.00
    << "\t\t with tax: $" << AddTax(10.00) << endl;
}
