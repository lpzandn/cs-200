//
//  main.cpp
//  Functions3
//
//  Created by AndresLopezBormann on 9/23/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

float CountChange (int quarters, int dimes, int nickels, int pennies)
{
    quarters = quarters * .25 ;
    dimes = dimes * .1;
    nickels = nickels * .05;
    pennies = pennies * .01;
    float total = quarters + dimes + nickels + pennies;
    return total;
}

int main ()
{
    cout << "COUNT CHANGE" << endl;
    
    while (true)
    {int quarters, dimes, nickels, pennies;
        
        cout << "How many Quarters?\t";
        cin >> quarters;
        cout << endl << "How many dimes\t";
        cin >> dimes;
        cout << endl << "How many nickels\t";
        cin >> nickels;
        cout << endl << "How many pennies \t";
        cin >> pennies;
        float result = CountChange (quarters, dimes, nickels, pennies);
        
        cout << "Results: " << result << endl;
    }

    return 0;
}
