//
//  main.cpp
//  Functions Lab p1
//
//  Created by AndresLopezBormann on 9/23/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
using namespace std;

float PercentToDecimal (float percent)
{
    float decimal = percent / 100;
    return decimal;
}

int main()

{

    float userPercent;
    float decimalVersion;

    cout << "Enter a percent value, without the %: ";
    cin >> userPercent;
    
    decimalVersion = PercentToDecimal (userPercent);
    cout << endl << "Decimal Value: " << decimalVersion;

    
    
    
    return 0;
}
