//
//  main.cpp
//  whileloopslab.cpp
//
//  Created by AndresLopezBormann on 9/16/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    int count = 0;
    while (count <=20)
    {
        cout << count << " ";
        count++;
    }
}

void Program2()
{
    int count = 1;
    while (count <=128)
    {
        cout << count << " ";
        count = count * 2;
    }
}

void Program3()
{
    int secretNumber = 13;
    int playerGuess;
    do{
        cout << "Guess a number: ";
        cin >> playerGuess;
         
              if (playerGuess > secretNumber)
              {cout << "Too high!" << endl;}
              if (playerGuess < secretNumber)
              {cout << "Too Low!" << endl;}
              if (playerGuess == secretNumber)
              {cout << "Thats right" << endl;}
          }
    while (playerGuess != secretNumber);
        cout << "Game Over";
      
}


void Program4()
{
    int testNumber = 0;
    cout << "Please enter a number between 1 and 5: ";
    cin >> testNumber;
    while (testNumber < 1 || testNumber > 5)
    {
        cout << endl << "Invalid entry, try again: ";
        cin >> testNumber;
        
    }
  
    cout << endl;
      cout << "Thank you.";
}

void Program5()
{
    float startingWage;
    float percentRaisePerYear;
    float adjustedWage;
    int yearsWorked;
    int yearCounter = 1;
    
    cout << "What is your starting wage? ";
    cin >> startingWage;
    adjustedWage = startingWage;
    cout << "What % raise do you get per year? ";
    cin >> percentRaisePerYear;
    cout << "How many years have you worked there? ";
    cin >> yearsWorked;
      cout << "Salary at year " << yearCounter << ":   " << adjustedWage << endl;
    while (yearsWorked != yearCounter){
    adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage;
       yearCounter++;
        cout << "Salary at year " << yearCounter << ":   " << adjustedWage << endl;
         
    }
}

void Program6()
{
    int n;
    int counter = 1;
    int sum = 0;
    cout << "Enter a value for n: ";
    cin >> n;
    while (counter <= n){
        sum +=counter;
        counter++;
        cout << "sum: "<<sum << endl;
    }
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
