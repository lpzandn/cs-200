#include <iostream> // console streams
#include <fstream>  // file streams
#include <string>   // strings
#include <cstdlib>  // random #s
#include <ctime>    // time
using namespace std;

// Function declarations -------------------------------------------------------------------------------------
void DisplayMenu( int viewBalance, int deposit, int withdraw, int update, int quit );
void DisplayAccounts();
int GetChoice( int min, int max );
float GetDollarAmount();
void ViewBalances( float checkingBalance, float savingsBalance );
void Deposit( float & checkingBalance, float & savingsBalance );
void Withdraw( float & checkingBalance, float & savingsBalance );
void UpdateUserInfo( int accountNum, string & email );
void LoadDataFile( string & email, int & accountNum, float & checkingBalance, float & savingsBalance );
void SaveDataFile( string email, int accountNum, float checkingBalance, float savingsBalance );

// -----------------------------------------------------------------------------------------------------------
int main()
{
    const int MENU_VIEW_BALANCE = 1, MENU_DEPOSIT = 2, MENU_WITHDRAW = 3, MENU_UPDATE = 4, MENU_QUIT = 5;

    bool done = false;

    string email;
    int accountNum;
    float checkingBalance;
    float savingsBalance;

    LoadDataFile( email, accountNum, checkingBalance, savingsBalance );

    cout << "BANK PROGRAM" << endl << endl;
    while ( !done )
    {
        DisplayMenu( MENU_VIEW_BALANCE, MENU_DEPOSIT, MENU_WITHDRAW, MENU_UPDATE, MENU_QUIT );
        int userChoice = GetChoice( 1, 5 );
        cout << endl;

        switch( userChoice )
        {
            case MENU_VIEW_BALANCE:
            ViewBalances( checkingBalance, savingsBalance );
            break;

            case MENU_DEPOSIT:
            Deposit( checkingBalance, savingsBalance );
            break;

            case MENU_WITHDRAW:
            Withdraw( checkingBalance, savingsBalance );
            break;

            case MENU_UPDATE:
            UpdateUserInfo( accountNum, email );
            break;

            case MENU_QUIT:
            done = true;
            break;
        }

        cout << "---------------------------------------" << endl;
        cout << endl << endl;
    }

    SaveDataFile( email, accountNum, checkingBalance, savingsBalance );

    return 0;
}

// Function definitions --------------------------------------------------------------------------------------
void DisplayMenu( int viewBalance, int deposit, int withdraw, int update, int quit )
{
    cout << viewBalance << ". View balances" << endl;
    cout << deposit     << ". Deposit" << endl;
    cout << withdraw    << ". Withdraw" << endl;
    cout << update      << ". Update user info" << endl;
    cout << quit        << ". End" << endl;
}

void DisplayAccounts()
{
    cout << "1. Checking" << endl;
    cout << "2. Savings" << endl;
}

int GetChoice( int min, int max )
{
    int choice;
    cout << ">> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid choice. Try again." << endl;
        cout << ">> ";
        cin >> choice;
    }
    return choice;
}

float GetDollarAmount()
{
    float choice;
    cout << ">> $";
    cin >> choice;

    while ( choice < 0 )
    {
        cout << "Cannot be negative! Try again." << endl;
        cout << ">> $";
        cin >> choice;
    }

    return choice;
}

void ViewBalances( float checkingBalance, float savingsBalance )
{
    cout << "Checking account: $" << checkingBalance << endl;
    cout << "Savings account:  $" << savingsBalance << endl;
}

void Deposit( float & checkingBalance, float & savingsBalance )
{
    cout << "Deposit into which?" << endl;
    DisplayAccounts();
    int choice = GetChoice( 1, 2 );

    cout << "Deposit how much? $";
    float amount = GetDollarAmount();

    if ( choice == 1 )
    {
        checkingBalance += amount;
    }
    else if ( choice == 2 )
    {
        savingsBalance += amount;
    }
}

void Withdraw( float & checkingBalance, float & savingsBalance )
{
    cout << "Deposit from which?" << endl;
    DisplayAccounts();
    int choice = GetChoice( 1, 2 );

    cout << "Withdraw how much? $";
    float amount = GetDollarAmount();

    if ( choice == 1 && amount <= checkingBalance )
    {
        checkingBalance -= amount;
    }
    else if ( choice == 2 && amount <= savingsBalance )
    {
        savingsBalance -= amount;
    }
    else
    {
        cout << "Not enough money!" << endl;
    }
}

void UpdateUserInfo( int accountNum, string & email )
{
    cout << "Account number: " << accountNum << endl;
    cout << "Old email:      " << email << endl;

    cout << endl << "Please enter a new email: ";
    cin >> email;
}

void LoadDataFile( string & email, int & accountNum, float & checkingBalance, float & savingsBalance )
{
    email = "unknown";
    srand( time( NULL ) );
    accountNum = rand() % 100000;
    checkingBalance = 0;
    savingsBalance = 0;
}

void SaveDataFile( string email, int accountNum, float checkingBalance, float savingsBalance )
{
    
}
