//
//  main.cpp
//  File IO Lab p2
//
//  Created by AndresLopezBormann on 9/30/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void DisplayMainMenu()
{
    cout << "Vote for a food item:" << endl;
    cout << "1. Pizza" << endl;
    cout << "2. Samosa" << endl;
    cout << "3. Tacos" << endl;
    cout << "4. Bibimbap" << endl;
    cout << endl << "Or, Quit the program with: \n5. Quit" << endl;
}

int GetChoice ( int min, int max )
{
    int choice;
    cout << ">> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid value. Try again." << endl;
        cout << ">> ";
        cin >> choice;
    }
return choice;
}

int main()
{
    ofstream output;
    output.open ("Poll Results.txt");
    
    bool done = false;
    int menuChoice;
    
    
    int pizza = 0;
    int samosas = 0;
    int tacos = 0;
    int bibimbap = 0;
    string todoItem;
    
    output <<"Poll Results:" << endl<< endl;
    while (!done)
    {
        DisplayMainMenu();
        menuChoice = GetChoice (1,5);
        if (menuChoice == 1)
        {
            pizza++;
        }
        else if (menuChoice == 2)
        {
            samosas++;
        }
        else if (menuChoice == 3)
        {
            tacos++;
        }
        else if (menuChoice == 4)
        {
            bibimbap++;
        }
        
        else if (menuChoice == 5)
        {
            cout << "GoodBye";
            output << "Pizza:\t\t" << pizza << "votes" << endl;
            output << "Samosas:\t" << samosas << "votes" << endl;
            output << "Tacos:\t\t" << tacos << "votes" << endl;
            output << "bibimbap:\t" << bibimbap << "votes" << endl;
            done = true;
        }
        
        
    }
    output.close();
    return 0;
}
