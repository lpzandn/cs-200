//j
//  main.cpp
//  File IO Lab p1
//
//  Created by AndresLopezBormann on 9/29/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void DisplayMainMenu()
{
    cout << "1. New to-do item" << endl;
    cout << "2. Save and quit" << endl;

}

int GetChoice ( int min, int max )
{
    int choice;
    cout << ">> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid value. Try again." << endl;
        cout << ">> ";
        cin >> choice;
    }
return choice;
}

        
    

string GetToDoItem ()
{
    string toDoItem;
    cout << "Enter to-do item: ";
    cin.ignore();
    getline(cin,toDoItem);
    return toDoItem;
}


int main()
{
    ofstream output;
    output.open ("todo.txt");
    
    int counter = 1;
    bool done = false;
    int menuChoice;
    string todoItem;
    
    output <<"To-Do:" << endl<< endl;
    while (!done)
    {
        DisplayMainMenu();
        menuChoice = GetChoice (1,2);
        if (menuChoice == 1)
        {
            todoItem = GetToDoItem();
            output << counter << ". " << todoItem << endl;
            counter++;
        }
        
        else if (menuChoice == 2)
        {
            cout << "GoodBye";
            done = true;
        }
        
        
    }
    output.close();
    return 0;
}
