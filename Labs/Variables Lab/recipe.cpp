//
//  main.cpp
//  recipe Lab
//
//  Created by AndresLopezBormann on 9/3/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
using namespace std;

int main()
{
    // declare ingredients variable
    // Deisplay ingredients  to the screen
    // Create  a variable for "batch size"
    // Asl the user how many batches to make
    // Recalculate ingredient amounts
    // Display updated ingredient list
    float cupsOfButter = 1;
    float cupsOfSugar = 1;
    float eggs = 2;
    float tspsVanilla = 2;
    float tspsBakingSoda = 1;
    float tspsSalt = 0.5;
    float cupsFlour = 3;
    float cupsChocolateChips = 2;
    
    cout << "Ingredient list:\n";
    cout << cupsOfButter << " cup(s) of Butter\n";
    cout << cupsOfSugar << " cup(s) of Sugar\n";
    cout << eggs << " egg(s)\n";
    cout << tspsVanilla << " tsp(s) of vanilla\n";
    cout << tspsBakingSoda << " tsp(s) of Baking Soda\n";
    cout << tspsSalt << " tsp(s) of salt\n";
    cout << cupsFlour << " cup(s) of flour \n";
    cout << cupsChocolateChips << " cup(s) of chocolate chips \n";
  
    cout << "How many batches?\n";
    float batchSize;
    cin >> batchSize;
    
    cupsOfButter = cupsOfButter * batchSize;
    cupsOfSugar = cupsOfSugar * batchSize;
    eggs = eggs * batchSize;
    tspsVanilla = tspsVanilla * batchSize;
    tspsBakingSoda = tspsBakingSoda * batchSize;
    tspsSalt = tspsSalt * batchSize;
    cupsFlour = cupsFlour * batchSize;
    cupsChocolateChips = cupsChocolateChips * batchSize;
    
    cout << "Adjusted ingredient list:\n\n";
    cout << cupsOfButter << " cup(s) of butter\n";
    cout << cupsOfSugar << " cup(s) of Sugar\n";
    cout << eggs << " egg(s)\n";
    cout << tspsVanilla << " tsp(s) of vanilla\n";
    cout << tspsBakingSoda << " tsp(s) of Baking Soda\n";
    cout << tspsSalt << " tsp(s) of salt\n";
    cout << cupsFlour << " cup(s) of flour \n";
    cout << cupsChocolateChips << " cup(s) of chocolate chips \n";
    return 0;
    
    
}
