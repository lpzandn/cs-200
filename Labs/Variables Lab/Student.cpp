//
//  main.cpp
//  Student
//
//  Created by AndresLopezBormann on 9/3/20.
//  Copyright © 2020 AndresLopezBormann. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;


int main() {
    // Declare and initialize variables
    // Display student info
    // Have user update info
    // Display updated info
    
    string email = "student@jccc.edu";
    char grade = 'A';
    float gpa = 3.5;
    int semestersCompleted = 2;
    bool degreeSeeking = true;
    
    cout << "Email:                   "<< email << endl;
    cout << "Grade:                   "<< grade << endl;
    cout << "GPA:                     "<< gpa << endl;
    cout << "Semesters completed:     "<< semestersCompleted << endl;
    cout << "Degree seeking?:         "<< degreeSeeking << endl << endl;
    
    cout << "Update info" << endl;
    
    cout << "Enter email: ";
    cin >> email;
    cout << "Enter grade (A, B, C, D, or F): " ;
    cin >> grade;
    cout << "Enter gpa: ";
    cin >> gpa;
    cout << "Enter Semesters Completed: ";
    cin >> semestersCompleted;
    cout << "Enter degree seeking (1 = yes, 0 = no): ";
    cin >> degreeSeeking;
    
    cout << "\nUpdated Student Information" << endl;
    
    cout << "Email:                   "<< email << endl;
    cout << "Grade:                   "<< grade << endl;
    cout << "GPA:                     "<< gpa << endl;
    cout << "Semesters completed:     "<< semestersCompleted << endl;
    cout << "Degree seeking?:         "<< degreeSeeking << endl << endl;
    return 0;
}
